from linecache import updatecache
from pickle import FALSE
import pygame
from pygame.locals import *
import time
from network import Network
from _thread import *
import pickle

pygame.init()
width = 600
height = 600
win = pygame.display.set_mode((width, height))
pygame.display.set_caption("Client")

clientNumber = 0

class Player():
    def __init__(self, x, y, width, height, color):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color
        self.rect = (x, y, width, height)
        self.vel = 1
        self.direction = 0
        self.pos = (0,0)
        self.n = Network()
        playerNum = self.n.id
        print("you are player ", playerNum)

    def draw(self, win, board):
        for x in range(30):
            for y in range(30):
                if board[x][y]==1:
                    rect = (x*20,y*20,20,20)
                    pygame.draw.rect(win, self.color, rect)
                if board[x][y]==2:
                    rect = (x*20,y*20,20,20)
                    pygame.draw.rect(win, (255, 0, 0), rect)
        # pygame.draw.rect(win, self.color, self.rect)

    def move(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.direction = 4
            self.n.sendInput("LEFT")
        if keys[pygame.K_RIGHT]:
            self.direction = 2
            self.n.sendInput("RIGHT")
        if keys[pygame.K_UP]:
            self.direction = 1
            self.n.sendInput("UP")
        if keys[pygame.K_DOWN]:
            self.direction = 3
            self.n.sendInput("DOWN")

    def updatePos(self):
        currentPos = self.pos
        if self.direction == 4:
            self.pos = (currentPos[0]-1, currentPos[1])
        if self.direction == 2:
            self.pos = (currentPos[0]+1, currentPos[1])
        if self.direction == 1:
            self.pos = (currentPos[0], currentPos[1]-1)
        if self.direction == 3:
            self.pos = (currentPos[0], currentPos[1]+1)

def redrawWindow(win, player, board):
    win.fill((0, 0, 0))
    player.draw(win, board)
    pygame.display.update()

def updateBoard(p, board):
    x = p.pos[0]
    y = p.pos[1]
    board[x][y] = 1

def update(p):
    p.move()
    p.updatePos()
    
def checkEnd(p, board):
    x = p.pos[0]
    y = p.pos[1]
    inboard = (x>=0 and x<100 and y>=0 and y<100)
    return p.direction!=0 and (board[x][y] != 0 or not inboard)


def threaded_client(conn, gameBoard):
    data = []
    while True:
        try:
            packet = conn.recv(4096)
            data.append(packet)
            try:
                result = pickle.loads(b"".join(data))
                gameBoard["board"] = result
                print(result[68])
                data = []
            except:
                pass
        except :
            break
    print("Lost Connection")
    conn.close()

def main():
    
    # Fill background
    background = pygame.Surface(win.get_size())
    background = background.convert()
    background.fill((0, 0, 0))
    # Display some text
    font = pygame.font.Font(None, 150)
    text = font.render("Game Over", 1, (255, 255, 255))
    textpos = text.get_rect()
    textpos.centerx = background.get_rect().centerx
    textpos.centery = background.get_rect().centery
    background.blit(text, textpos)
    while True:
        board = [[0]*30 for i in range(30)]
        clock = pygame.time.Clock()
        run = True
        p = Player(0,0,50,50,(0,255,255))

        gameBoard = {}
        gameBoard["board"] = board
        start_new_thread(threaded_client, (p.n.client, gameBoard))
        while True: 
            print("hello")
            time.sleep(1)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    pygame.quit()

            # p.move()
            data = p.n.send("get")
            # run = not checkEnd(p, board)
            if data:
                print(data)
                # updateBoard(p, board)
                board = data
                # redrawWindow(win, p, board)

                win.fill((0, 0, 0))
                p.draw(win, board)
                pygame.display.update()
            else:
                print("data is empty")


                
        restart = False
        while not restart: 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    print("quiting game")
                    run = False
                    pygame.quit()
            keys = pygame.key.get_pressed()
            if keys[pygame.K_RETURN]:
                restart = True
                print("ENTER WAS PRESSED")
                
            win.blit(background, (0, 0))
            pygame.display.flip()

#  while gameisrunning:
#     sendplayersinput
#     readgamestatefromserver
#     check if current frame should be rendered
#         rendergamestate


main()