from concurrent.futures import thread
import socket
from _thread import *
import time
import sys
import pickle
from game import Game

class ServerPlayer:
    def __init__(self, id, pos, action, conn):
        self.id = id
        self.alive = True
        self.pos = pos
        self.action = action
        self.conn = conn
    
    def sendToPlayer(self, gameState):
        data = pickle.dumps(gameState)
        self.conn.sendall(data)


server = "127.0.0.1"
port = 5555

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    s.bind((server, port))
except socket.error as e:
    str(e)

connectionNum = 2
s.listen(connectionNum)
print("Waiting for connection, Server Started")

def threaded_client(conn, currentPlayer, playersInput):
    conn.send(str.encode(str(currentPlayer)))
    reply = ""
    while True:
        try:
            data = conn.recv(2048)
            reply = data.decode("utf-8")

            if not data: 
                print("Disconnected")
                break
            else:
                playersInput["action"] = reply
                print("Received: ", reply)
                print("Sending: ", reply)
            
            data = pickle.dumps(boardList[1])
            print("data:", len(data))
            conn.sendall(data)
        except :
            break
    print("Lost Connection")
    conn.close()


currentPlayer = 0
players = []
startingPos = [(33,50),(68,50)]
boardList = {}
boardList[1]=[[0]*30 for i in range(30)]
boardList[1][0][0]=1
while True:
    conn, addr = s.accept()
    print("Connected to:", addr)

    currentPlayer+=1
    playerAction = {}
    playerAction["action"] = "" 
    start_new_thread(threaded_client, (conn, currentPlayer, playerAction))
    players.append(ServerPlayer(currentPlayer, startingPos[currentPlayer-1], playerAction, conn))
    # if currentPlayer == 2:
    #     game = Game(players)
    #     game.start()
    