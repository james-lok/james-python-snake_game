import socket
import pickle

class Network:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = "127.0.0.1"
        self.port = 5555
        self.addr = (self.server, self.port)
        self.id = self.connect()
        print(self.id)


    def connect(self):
        try:
            self.client.connect(self.addr)
            return self.client.recv(2048).decode()
        except:
            pass


    def send(self, data):
        try: 
            self.client.send(str.encode(data))
            return pickle.loads(self.client.recv(2048*2))
        except socket.error as e:
            print(e)

    def sendInput(self, data):
        try: 
            self.client.send(str.encode(data))
        except socket.error as e:
            print(e)

    def receive(self):
        try: 
            data = []
            while True:
                print ("receiving data")
                packet = self.client.recv(4096)
                data.append(packet)
                try:
                    result = pickle.loads(b"".join(data))
                    return result
                except:
                    pass
                
            # data_arr = pickle.loads(b"".join(data))
            # return data_arr
            # packet = self.client.recv(20480)
            # if not packet:
            #     return "empty"
            # return pickle.loads(self.client.recv(20480))
        except socket.error as e:
            print(e)
 