class Game:
    def __init__(self, players):
        # initiate board
        self.board = [[0]*100 for i in range(100)]
        self.fps = 1
        self.players = players

    def getPlayersInput(self):
        for player in self.players:
            action = player.action['action']
            if player.alive and self.hasMoved(action):
                currentPosX = player.pos[0]
                currentPosY = player.pos[1]
                
                nextPosX = currentPosX 
                nextPosY = currentPosY

                print(player.pos)
                if action == "UP":
                    nextPosY = currentPosY-1
                if action == "RIGHT":
                    nextPosX = currentPosX+1
                if action == "DOWN":
                    nextPosY = currentPosY+1
                if action == "LEFT":
                    nextPosX = currentPosX-1

                if ((nextPosX >=0 and nextPosX<100 and nextPosY >=0 and nextPosY<100) and (self.board[nextPosX][nextPosY]==0)) :
                    # update board
                    self.board[nextPosX][nextPosY] = player.id
                    # update player pos
                    player.pos = (nextPosX, nextPosY)
                else :
                    player.alive = False

    def start(self):
        # init board
        for player in self.players:
            x = player.pos[0]
            y = player.pos[1]
            self.board[x][y] = player.id

        time.sleep(1)
        # gameloop
        frameTime = 1/self.fps
        run = True
        lasttime = time.time()
        count = 0
        while run: 
            # get players inputs
            
            now = time.time()
            duration = now - lasttime

            if (duration>=frameTime):
                # update game from input

                self.getPlayersInput()
                # check if player has died

                # check if game is over
                self.board[count][0] = 1

                if count<9:
                    count = count+1
                # send update to game
                self.sendGameState(self.board)
                # update(p)
                # run = not checkEnd(p, board)
                
                # updateBoard(p, board)
                # redrawWindow(win, p, board)
                lasttime = time.time()
    
    def hasMoved(self, action):
        return action == "UP" or action == "RIGHT" or action == "DOWN" or action == "RIGHT"

    def sendGameState(self, board):
        for player in self.players:
            print (player.id)
            print (board[68])
            player.sendToPlayer(board)